data {
  int numyears;             // how many years/periods there are in the simulation
  real N1;                  // initial population of the species
  real mean_lambda;         // \bar\lambda: mean population growth rate
  real sigma2_lambda;       // temporal variation in growth rate
  real sigma2_y;            // variance of observation errors
  real beta1;               // linear impact of the control
  real beta2;               // quadratic approach of the control
  real K;                   // carrying capacity
  real N_hat;               // point at which effectiveness of the control peaks
  // policy information
  int<lower=0> policies;
  vector[policies] prob_control;        // probability that the control is applied
  vector[policies] threshold_control;   // threshold at which the control is applied
}
generated quantities {
  vector[numyears]      N[policies];            // true population levels
  vector[numyears]      y[policies];            // observed population levels
  vector[numyears - 1]  z[policies];            // whether the control was applied
  vector[numyears - 1]  lambda[policies];       // the growth rate
  
  for(p in 1:policies){
    // initialization
    N[p][1] = N1;
    // simulate actual population growth
    for(t in 1:(numyears - 1)){
      if(N[p][t] >= threshold_control[p]){
        z[p][t] = binomial_rng(1, prob_control[p]);  // decide randomly whether to take the control action
      }else{
        z[p][t] = 0; }
      
      lambda[p][t] = normal_rng(fmax(0, mean_lambda - z[p][t]*N[p][t]*beta1 - 
                                                   z[p][t]*pow(fmax(N[p][t] - N_hat,0), 2)*beta2), 
                             sqrt(sigma2_lambda));  // adjust the mean rate by the control variable
      N[p][t+1] =  fmax(0, fmin(N[p][t] * lambda[p][t], K));   // logistic growth model: (K - N[t])/K  
    }
    // generate observations from the population
    for(t in 1:numyears){
      y[p][t] = normal_rng(N[p][t], sqrt(sigma2_y));
    }  
  }
}
