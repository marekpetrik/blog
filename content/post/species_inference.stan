data {
  int<lower=0> numyears;
  vector[numyears] y;           // observation of the population (noisy)
  vector[numyears-1] z;         // perfect observation of the control
}
parameters {
  real<lower=0,upper=1.3> mean_lambda;                      // Mean growth rate - without control applied
  real<lower=0,upper=0.05> sigma_proc;                       // SD of state process
  real<lower=0,upper=25> sigma_obs;                      // SD of observation process
  vector<lower=0>[numyears - 1] lambda;                   // actual population growth rate
  real<lower=30,upper=70> N_est1;                         // Initial population size
  real<lower=-0.01,upper=0.01> beta1;                     // linear impact of the control
  real<lower=-0.000005,upper=0.000005> beta2;             // quadratic approach of the control  
}
transformed parameters {
  vector<lower=0, upper=2>[numyears - 1] lambda_control;    // Mean growth rate after control application
  vector<lower=0>[numyears] N_est;              // estimated mean of the population
  
  N_est[1] = N_est1;
  
  // State process
  for (t in 1:(numyears - 1)){
    lambda_control[t] = mean_lambda - z[t] * N_est[t] * beta1 - 
      z[t] * pow( fmax(N_est[t] - 300, 0), 2) * beta2;
    N_est[t + 1] = N_est[t] * lambda[t];
  }
}
model {
  // It is possible to provide priors on values of model parameters here
  //beta1 ~ normal(0, 0.1);
  //beta2 ~ normal(0, 0.01);
  
  // Likelihood
  lambda ~ normal(lambda_control, sigma_proc);
  // Observation process
  y ~ normal(N_est, sigma_obs);
}

generated quantities {
  real<lower=0> sigma2_obs;
  real<lower=0> sigma2_proc;
  
  sigma2_obs = square(sigma_obs);
  sigma2_proc = square(sigma_proc);
}

